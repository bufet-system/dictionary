#CHANGELOG
 
 - **version**: 0.4.3  
   **issue**: [library#4](https://gitlab.com/bufet-system/dictionary/-/issues/4)  
   **description**: add project README
 
 - **version**: 0.4.2  
   **issue**: [general#3](https://gitlab.com/bufet-system/general/-/issues/3)  
   **description**: fix ingress path
 
 - **version**: 0.4.1  
   **issue**: [general#3](https://gitlab.com/bufet-system/general/-/issues/3)  
   **description**: fix deploying configuration for kubernetes/ingress
 
 - [0.4.0]
   [#6](https://gitlab.com/bufet-system/dictionary/issues/6) - add endpoint to remove recipe

 - [0.3.0]
   [#7](https://gitlab.com/bufet-system/dictionary/issues/7) - add service to return recipe description

 - [0.2.1]
   [#8](https://gitlab.com/bufet-system/dictionary/issues/8) - fix maven repo

 - [0.2.0]
   [#3](https://gitlab.com/bufet-system/dictionary/issues/3) - add CI and kubernetes configuration

 - [0.1.0]
   [#2](https://gitlab.com/bufet-system/dictionary/issues/2) - add service to return recipes

 - [0.0.1]
   [#1](https://gitlab.com/bufet-system/dictionary/issues/1) - add default project structure
