pipeline {
  agent any
  stages {
    stage('Compile Stage') {
      steps {
        withMaven(maven: 'bufet-maven') {
          sh 'mvn clean package'
        }
      }
    }

    stage('Testing Stage') {
      steps {
        withMaven(maven: 'bufet-maven') {
          sh 'mvn test'
        }
      }
    }

    stage('Code Analysis Stage') {
      steps {
        withMaven(maven: 'bufet-maven') {
          sh 'mvn sonar:sonar \
                  -Dsonar.projectKey=bufer-system \
                  -Dsonar.organization=bufet-system \
                  -Dsonar.host.url=https://sonarcloud.io \
                  -Dsonar.login=127088cc4a9e7893c59226a2021fdb20e09eac3d'
        }
      }
    }

    stage('Build Image') {
      when {
        branch 'master'
      }
      steps {
        sh 'docker build -t library .'
      }
    }

    stage('Push Image to Registry') {
      when {
        branch 'master'
      }
      steps {
        withDockerRegistry([credentialsId: "docker-registry", url: ""]) {
          sh 'docker tag library:latest bufetsystem/library'
          sh 'docker push bufetsystem/library:latest'
        }
      }
    }

    stage('Deploy artifacts to Artifactory Stage') {
      when {
        branch 'master'
      }
      steps {
        withMaven(maven: 'bufet-maven') {
          configFileProvider([configFile(fileId: 'maven_settings', variable: 'SETTINGS')]) {
            sh "mvn -s $SETTINGS deploy -DskipTests -Dartifactory_url=http://213.32.64.129:8080/artifactory"
          }
        }
      }
    }

    stage('Deploy To DEV Environment') {
      when {
        branch 'master'
      }
      steps {
        sh 'docker rm -f library'
        sh 'docker run -d --name=library --publish=8091:8091 library:latest '
      }
    }
  }

  post {
    success {
      slackSend(color: '#00FF00', message: "SUCCESSFUL: Job '${env.JOB_NAME} [${env.BUILD_NUMBER}]' (${env.BUILD_URL})")
    }

    failure {
      slackSend(color: '#FF0000', message: "FAILED: Job '${env.JOB_NAME} [${env.BUILD_NUMBER}]' (${env.BUILD_URL})")
    }
  }
}
