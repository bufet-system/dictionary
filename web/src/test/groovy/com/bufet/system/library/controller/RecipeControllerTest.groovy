package com.bufet.system.library.controller

import com.bufet.system.library.controller.response.RecipeControllerResponses
import com.bufet.system.library.dto.*
import com.bufet.system.library.response.recipe.GetRecipeListResponse
import com.bufet.system.library.response.recipe.GetRecipeResponse
import com.bufet.system.library.service.RecipeService
import org.springframework.test.web.servlet.MockMvc
import spock.lang.Specification

import static com.bufet.system.library.model.RecipeDifficulty.EASY
import static com.bufet.system.library.model.UnitsOfMeasurement.ITEM
import static java.util.Collections.singletonList
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.standaloneSetup

/**
 * Tests for controller:  {@link RecipeController}
 * <p>
 * Created by Dawid Cisowski
 * on 09.06.19.
 * Buffet System
 */
class RecipeControllerTest extends Specification {

    def recipeService = Mock(RecipeService)
    def recipeController = new RecipeController(recipeService)

    GetRecipeDto getRecipeDto = null
    GetRecipeListDto getRecipeListDto = null
    private MockMvc mockMvc

    def setup() {
        mockMvc = standaloneSetup(recipeController).build()
        buildModels()
    }


    def "should return status OK when call to get recipes endpoint"() {
        given:

        when:
        def response = mockMvc.perform(get("/recipes"))

        then:
        response.andExpect(status().isOk())
    }

    def "should success response list of recipes"() {
        given:
        GetRecipeListResponse getRecipesResponse = GetRecipeListResponse.of(singletonList(getRecipeListDto))

        when:
        def response = mockMvc.perform(get("/recipes"))

        then:
        1 * recipeService.getRecipes() >> getRecipesResponse

        and:
        response.andExpect(status().isOk())
                .andExpect(content().json(RecipeControllerResponses.GET_RECIPE_LIST_RESPONSE_JSON))
    }

    def "should return status OK when call to get recipe endpoint"() {
        given:

        when:
        def response = mockMvc.perform(get("/recipes/1"))

        then:
        response.andExpect(status().isOk())
    }

    def "should success response recipe"() {
        given:
        GetRecipeResponse getRecipeResponse = GetRecipeResponse.of(getRecipeDto)

        when:
        def response = mockMvc.perform(get("/recipes/1"))

        then:
        1 * recipeService.getRecipe('1') >> getRecipeResponse

        and:
        response.andExpect(status().isOk())
                .andExpect(content().json(RecipeControllerResponses.GET_RECIPE_RESPONSE_JSON))
    }

    def "should success remove product"() {
        when:
        def response = mockMvc.perform(delete("/recipes/1"))

        then:
        1 * recipeService.removeRecipe('1')

        and:
        response.andExpect(status().isOk())
    }

    void buildModels() {
        getRecipeDto = GetRecipeDto.builder()
                .id("1")
                .name("Test Recipe")
                .description("Description")
                .imagePreviewSrc("image.png")
                .timeToPrepare(120L)
                .author("Jan Kowalski")
                .tips(singletonList(GetRecipeTip.of("1", "tip")))
                .steps(singletonList(
                        GetRecipeStep.of("1", null, "STEP")))
                .ingredients(singletonList(
                        GetIngredientDto.of("1", "INGREDIENT", 3, ITEM)
                ))
                .recipeDifficulty(EASY)
                .build()

        getRecipeListDto = GetRecipeListDto.builder()
                .id("1")
                .name("Test Recipe")
                .description("Description")
                .imagePreviewSrc("image.png")
                .timeToPrepare(120L)
                .author("Jan Kowalski")
                .recipeDifficulty(EASY)
                .build()
    }
}