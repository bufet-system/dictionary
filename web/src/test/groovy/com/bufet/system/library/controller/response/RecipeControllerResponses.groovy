package com.bufet.system.library.controller.response

class RecipeControllerResponses {

    static String GET_RECIPE_RESPONSE_JSON = '{\n' +
            '  "recipe": {\n' +
            '      "id": "1",\n' +
            '      "name": "Test Recipe",\n' +
            '      "description": "Description",\n' +
            '      "author": "Jan Kowalski",\n' +
            '      "imagePreviewSrc": "image.png",\n' +
            '      "ingredients": [\n' +
            '        {\n' +
            '          "id": "1",\n' +
            '          "name": "INGREDIENT",\n' +
            '          "amount": 3,\n' +
            '          "unitsOfMeasurement": "ITEM"\n' +
            '        }\n' +
            '      ],\n' +
            '      "steps": [\n' +
            '        {\n' +
            '          "id": "1",\n' +
            '          "imagePreviewSrc": null,\n' +
            '          "content": "STEP"\n' +
            '        }\n' +
            '      ],\n' +
            '      "recipeDifficulty": "EASY",\n' +
            '      "timeToPrepare": 120,\n' +
            '      "tips": [\n' +
            '        {\n' +
            '          "id": "1",\n' +
            '          "content": "tip"\n' +
            '        }\n' +
            '      ]\n' +
            '    }\n' +
            '}'

    static String GET_RECIPE_LIST_RESPONSE_JSON = '{\n' +
            '  "recipes": [\n' +
            '   {\n' +
            '      "id": "1",\n' +
            '      "name": "Test Recipe",\n' +
            '      "description": "Description",\n' +
            '      "author": "Jan Kowalski",\n' +
            '      "imagePreviewSrc": "image.png",\n' +
            '      "recipeDifficulty": "EASY",\n' +
            '      "timeToPrepare": 120\n' +
            '   }]\n' +
            '}'
}