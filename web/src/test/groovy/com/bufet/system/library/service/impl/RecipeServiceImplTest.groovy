package com.bufet.system.library.service.impl

import com.bufet.system.library.dto.*
import com.bufet.system.library.exception.RecipeNotExistException
import com.bufet.system.library.model.Recipe
import com.bufet.system.library.model.RecipeIngredient
import com.bufet.system.library.model.RecipeStep
import com.bufet.system.library.model.RecipeTip
import com.bufet.system.library.repository.RecipeRepository
import com.bufet.system.library.response.recipe.GetRecipeListResponse
import com.bufet.system.library.response.recipe.GetRecipeResponse
import com.bufet.system.library.service.RecipeService
import spock.lang.Specification

import static com.bufet.system.library.model.RecipeDifficulty.EASY
import static com.bufet.system.library.model.UnitsOfMeasurement.ITEM
import static java.util.Collections.singletonList

/**
 * Tests for service:  {@link RecipeServiceImpl}
 * <p>
 * Created by Dawid Cisowski
 * on 09.06.19.
 * Buffet System
 */
class RecipeServiceImplTest extends Specification {

    private RecipeRepository recipeRepository = Mock()
    private RecipeService recipeService = new RecipeServiceImpl(recipeRepository)

    private Recipe recipe
    private GetRecipeDto getRecipeDto
    private GetRecipeListDto getRecipeListDto

    def setup() {
        buildModels()
    }

    def "'GetAllRecipes' should return correct categories list"() {
        given:
        1 * recipeRepository.findAll() >> [recipe]

        when:
        GetRecipeListResponse getRecipesResponse = recipeService.getRecipes()

        then:
        getRecipesResponse.recipes == [getRecipeListDto]
    }

    def "'getRecipe' should throw NullPointerException and dont invoke 'findById'"() {
        given:

        when:
        recipeService.getRecipe(null)

        then:
        0 * recipeRepository.findById(_ as Long)

        and:
        thrown NullPointerException
    }

    def "'getRecipe' successful return recipe"() {
        given:
        String id = 1

        when:
        GetRecipeResponse getRecipeResponse = recipeService.getRecipe(id)

        then:
        1 * recipeRepository.findById(id) >> Optional.of(recipe)

        and:
        getRecipeResponse.recipe == getRecipeDto
    }

    def "'getRecipe' should throw RecipeNotExistException when recipe by given id not exist"() {
        given:
        String id = 1

        when:
        recipeService.getRecipe(id)

        then:
        1 * recipeRepository.findById(id) >> Optional.empty()

        and:
        thrown(RecipeNotExistException)
    }

    def "'Remove Recipe' should throw #RecipeNotExistException"() {
        given:

        when:
        recipeService.removeRecipe('1')

        then:
        1 * recipeRepository.existsById('1') >> false

        and:
        thrown RecipeNotExistException
    }

    def "'Remove recipe' should no error thrown"() {
        given:

        when:
        recipeService.removeRecipe('1')

        then:
        1 * recipeRepository.existsById('1') >> true

        and:
        noExceptionThrown()
    }

    void buildModels() {
        recipe = Recipe.builder()
                .id("1")
                .name("Test Recipe")
                .description("Description")
                .imagePreviewSrc("image.png")
                .timeToPrepare(120L)
                .author("Jan Kowalski")
                .tips(singletonList(RecipeTip.of("1", "tip")))
                .steps(singletonList(
                        RecipeStep.of("1", null, "STEP")))
                .ingredients(singletonList(
                        RecipeIngredient.of("1", "INGREDIENT", 3, ITEM)
                ))
                .recipeDifficulty(EASY)
                .build()

        getRecipeDto = GetRecipeDto.builder()
                .id("1")
                .name("Test Recipe")
                .description("Description")
                .imagePreviewSrc("image.png")
                .timeToPrepare(120L)
                .author("Jan Kowalski")
                .tips(singletonList(GetRecipeTip.of("1", "tip")))
                .steps(singletonList(
                        GetRecipeStep.of("1", null, "STEP")))
                .ingredients(singletonList(
                        GetIngredientDto.of("1", "INGREDIENT", 3, ITEM)
                ))
                .recipeDifficulty(EASY)
                .build()


        getRecipeListDto = GetRecipeListDto.builder()
                .id("1")
                .name("Test Recipe")
                .description("Description")
                .imagePreviewSrc("image.png")
                .timeToPrepare(120L)
                .author("Jan Kowalski")
                .recipeDifficulty(EASY)
                .build()
    }
}