package com.bufet.system.library.dto;

import com.bufet.system.library.model.RecipeDifficulty;
import com.bufet.system.library.service.RecipeService;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Dto Recipe model for service: {@link RecipeService#getRecipes()}
 * <p>
 * Created by Dawid Cisowski
 * on 27.02.20.
 * Buffet System
 */
@Data
@Builder
@ApiModel
@NoArgsConstructor
@AllArgsConstructor(staticName = "of")
public class GetRecipeListDto {

    @ApiModelProperty(position = 1, value = "Recipe identifier")
    private String id;

    @ApiModelProperty(position = 2, value = "Recipe name")
    private String name;

    @ApiModelProperty(position = 3, value = "Recipe description")
    private String description;

    @ApiModelProperty(position = 4, value = "Author of recipe")
    private String author;

    @ApiModelProperty(position = 5, value = "Preview image for recipe")
    private String imagePreviewSrc;

    @ApiModelProperty(position = 8, value = "Recipe difficulty")
    private RecipeDifficulty recipeDifficulty;

    @ApiModelProperty(position = 9, value = "Time to prepare Recipe in minutes")
    private Long timeToPrepare;
}
