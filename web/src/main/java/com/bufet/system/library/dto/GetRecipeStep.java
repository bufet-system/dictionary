package com.bufet.system.library.dto;

import com.bufet.system.library.service.RecipeService;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Dto Recipe step model for service: {@link RecipeService#getRecipes()}
 * <p>
 * Created by Dawid Cisowski
 * on 09.06.19.
 * Buffet System
 */
@Data
@ApiModel
@NoArgsConstructor
@AllArgsConstructor(staticName = "of")
public class GetRecipeStep {

  @ApiModelProperty(position = 1, value = "Recipe step identifier")
  private String id;

  @ApiModelProperty(position = 5, value = "Preview image for recipe step")
  private String imagePreviewSrc;

  @ApiModelProperty(position = 1, value = "Recipe step content")
  private String content;
}
