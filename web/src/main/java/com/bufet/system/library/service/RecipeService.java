package com.bufet.system.library.service;

/**
 * Buffet System
 * Created by Dawid Cisowski
 * on 15.09.19.
 */

import com.bufet.system.library.response.recipe.GetRecipeListResponse;
import com.bufet.system.library.response.recipe.GetRecipeResponse;
import lombok.NonNull;

import javax.validation.constraints.NotNull;

/**
 * Service to managing recipes
 */
public interface RecipeService {

    /**
     * Return list of all recipes in library
     *
     * @return response with list of all recipes{@link GetRecipeListResponse}
     */
    GetRecipeListResponse getRecipes();

    /**
     * Return recipe with all information
     *
     * @return response with recipe {@link GetRecipeResponse}
     */
    GetRecipeResponse getRecipe(@NonNull String id);

    /**
     * Remove recipe
     *
     * @param id id removing recipe
     */
    void removeRecipe(@NotNull String id);
}