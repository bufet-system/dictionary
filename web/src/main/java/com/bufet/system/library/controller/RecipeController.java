package com.bufet.system.library.controller;

/**
 * Buffet System
 * Created by Dawid Cisowski
 * on 15.09.19.
 */

import com.bufet.system.library.response.recipe.GetRecipeListResponse;
import com.bufet.system.library.response.recipe.GetRecipeResponse;
import com.bufet.system.library.service.RecipeService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RestController
@RequiredArgsConstructor
@RequestMapping("/recipes")
public class RecipeController {

    private final RecipeService recipeService;

    @GetMapping()
    @ApiOperation(value = "Return list of recipes in library", nickname = "Return list of recipes in library")
    public GetRecipeListResponse getRecipes() {
        return recipeService.getRecipes();
    }

    @GetMapping("/{id}")
    @ApiResponses(value = {
            @ApiResponse(code = 404, message = "[RECIPE_NOT_EXIST] Recipe not exist"),
    })
    @ApiOperation(value = "Return recipe description", nickname = "Return recipe description")
    public GetRecipeResponse getRecipe(@PathVariable String id) {
        return recipeService.getRecipe(id);
    }

    @DeleteMapping("/{id}")
    @ApiResponses(value = {
            @ApiResponse(code = 404, message = "[RECIPE_NOT_EXIST] Recipe not exist"),
    })
    @ApiOperation(value = "Delete recipe", nickname = "Delete recipe")
    public void removeRecipe(@PathVariable String id) {
        recipeService.removeRecipe(id);
    }
}