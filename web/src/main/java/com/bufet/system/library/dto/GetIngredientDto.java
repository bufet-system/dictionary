package com.bufet.system.library.dto;

import com.bufet.system.library.model.UnitsOfMeasurement;
import com.bufet.system.library.service.RecipeService;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Dto Ingredient model for service: {@link RecipeService#getRecipes()}
 * <p>
 * Created by Dawid Cisowski
 * on 09.06.19.
 * Buffet System
 */
@Data
@ApiModel
@NoArgsConstructor
@AllArgsConstructor(staticName = "of")
public class GetIngredientDto {

  @ApiModelProperty(position = 1, value = "Recipe ingredient identifier")
  private String id;

  @ApiModelProperty(position = 2, value = "Recipe ingredient name")
  private String name;

  @ApiModelProperty(position = 3, value = "Recipe ingredient amount")
  private int amount;

  @ApiModelProperty(position = 4, value = "Unit of measurement for product")
  private UnitsOfMeasurement unitsOfMeasurement;
}
