package com.bufet.system.library.dto;

import com.bufet.system.library.service.RecipeService;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Dto Recipe tip for service: {@link RecipeService#getRecipes()}
 * <p>
 * Created by Dawid Cisowski
 * on 09.06.19.
 * Buffet System
 */
@Data
@ApiModel
@NoArgsConstructor
@AllArgsConstructor(staticName = "of")
public class GetRecipeTip {
  @ApiModelProperty(position = 1, value = "Recipe tip identifier")
  private String id;

  @ApiModelProperty(position = 1, value = "Recipe tip content")
  private String content;
}
