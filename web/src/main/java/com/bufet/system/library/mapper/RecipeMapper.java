package com.bufet.system.library.mapper;

import com.bufet.system.library.dto.GetRecipeDto;
import com.bufet.system.library.dto.GetRecipeListDto;
import com.bufet.system.library.model.Recipe;
import org.mapstruct.Mapper;

/**
 * MapStruct mapper to map between Recipe and Recipe DTO models
 */
@Mapper
public interface RecipeMapper {

    /**
     * Map {@link Recipe} model to {@link GetRecipeDto} model
     *
     * @param recipe Recipe model to mapping
     * @return {@link GetRecipeDto} model
     */
    GetRecipeDto mapToGetRecipeDto(Recipe recipe);

    /**
     * Map {@link Recipe} model to {@link GetRecipeListDto} model
     *
     * @param recipe Recipe model to mapping
     * @return {@link GetRecipeListDto} model
     */
    GetRecipeListDto mapToGetRecipeListDto(Recipe recipe);
}