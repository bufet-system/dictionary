package com.bufet.system.library.response.recipe;

/**
 * Buffet System
 * Created by Dawid Cisowski
 * on 27.02.20.
 */

import com.bufet.system.library.dto.GetRecipeListDto;
import com.bufet.system.library.service.RecipeService;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.List;

/**
 * Response model of {@link RecipeService#getRecipes()} service
 */
@Data
@ApiModel
@AllArgsConstructor(staticName = "of")
public class GetRecipeListResponse {

    @ApiModelProperty(value = "List of all recipes in Library")
    private List<GetRecipeListDto> recipes;
}
