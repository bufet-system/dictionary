package com.bufet.system.library;

/**
 * Bufet System
 * Created by Dawid Cisowski
 * on 15.09.19.
 */

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LibraryApplication {

  public static void main(String[] args) {
    SpringApplication.run(LibraryApplication.class, args);
  }

}
