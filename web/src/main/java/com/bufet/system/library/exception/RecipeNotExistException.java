package com.bufet.system.library.exception;

/**
 * Buffet System
 * Created by Dawid Cisowski
 * on 27.02.20.
 */

import me.alidg.errors.annotation.ExceptionMapping;
import me.alidg.errors.annotation.ExposeAsArg;

import static org.springframework.http.HttpStatus.NOT_FOUND;


/**
 * Exception to mapping RECIPE_NOT_EXIST error
 */

@ExceptionMapping(statusCode = NOT_FOUND, errorCode = "RECIPE_NOT_EXIST")
public class RecipeNotExistException extends RuntimeException {
    private static final long serialVersionUID = 8513556996716752310L;

    @ExposeAsArg(0)
    private final String id;

    public RecipeNotExistException(String id) {
        super("Recipe with id:" + id + " not exist");
        this.id = id;
    }
}
