package com.bufet.system.library.app.runner;

import com.bufet.system.library.model.Recipe;
import com.bufet.system.library.model.RecipeIngredient;
import com.bufet.system.library.model.RecipeStep;
import com.bufet.system.library.model.RecipeTip;
import com.bufet.system.library.repository.RecipeRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.java.Log;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

import java.time.Duration;

import static com.bufet.system.library.model.RecipeDifficulty.EASY;
import static com.bufet.system.library.model.UnitsOfMeasurement.*;
import static java.util.Arrays.asList;
import static java.util.Collections.singletonList;



/**
 * Runner for Library App.
 * <p>
 * Buffet System
 * Created by Dawid Cisowski
 * on 28.09.19.
 */


@Log
@Component
@RequiredArgsConstructor
public class AppRunner implements ApplicationRunner {

  private final RecipeRepository recipeRepository;

  @Override
  public void run(ApplicationArguments args) throws Exception {
    recipeRepository.save(Recipe.builder()
            .id("1")
            .name("Grandmother's Oatmeal Cookies")
            .description("This is the best Oatmeal Cookie I have ever tasted and is my family's favorite. This is a recipe that I have had for years that a friend of mine gave me.")
            .imagePreviewSrc("https://images.media-allrecipes.com/userphotos/560x315/1527629.jpg")
            .timeToPrepare(120L)
            .author("Jan Kowalski")
            .tips(singletonList(RecipeTip.of("1", "serve with milk")))
            .steps(asList(
                    RecipeStep.of("1", null, "Beat eggs, and stir in raisins and vanilla. Refrigerate for at least an hour."),
                    RecipeStep.of("2", null, "Preheat oven to 350 degrees F (175 degrees C)."),
                    RecipeStep.of("3", null, "Cream together shortening, brown sugar, and white sugar until light and fluffy. Combine flour, baking soda, salt, and cinnamon; stir into the sugar mixture. Mix in raisins and eggs, then stir in oats and walnuts. Roll dough into walnut sized balls, and place 2 inches apart on ungreased cookie sheets"),
                    RecipeStep.of("4", null, "Bake for 10 to 12 minutes in preheated oven, or until edges are golden. Cool on wire racks.")))
            .ingredients(asList(
                    RecipeIngredient.of("1", "Egg", 3, ITEM),
                    RecipeIngredient.of("2", "Raisins", 1, CUP),
                    RecipeIngredient.of("3", "Vanilla extract", 1, TEA_SPOON),
                    RecipeIngredient.of("4", "Butter flavored shortening", 1, CUP),
                    RecipeIngredient.of("5", "Packed brown sugar", 1, CUP),
                    RecipeIngredient.of("6", "White sugar", 1, CUP),
                    RecipeIngredient.of("7", "all-purpose flour", 2.5, CUP),
                    RecipeIngredient.of("8", "Baking soda", 2, TEA_SPOON),
                    RecipeIngredient.of("9", "Teaspoon salt", 1, TEA_SPOON),
                    RecipeIngredient.of("10", "Teaspoon ground cinnamon", 1, TEA_SPOON),
                    RecipeIngredient.of("11", "Quick cooking oats", 2, CUP),
                    RecipeIngredient.of("12", "chopped walnuts", 2, CUP)
            ))
            .recipeDifficulty(EASY)
            .build());
  }
}
