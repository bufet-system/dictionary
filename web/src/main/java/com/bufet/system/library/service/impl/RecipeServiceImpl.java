package com.bufet.system.library.service.impl;

import com.bufet.system.library.dto.GetRecipeDto;
import com.bufet.system.library.dto.GetRecipeListDto;
import com.bufet.system.library.exception.RecipeNotExistException;
import com.bufet.system.library.mapper.RecipeMapper;
import com.bufet.system.library.repository.RecipeRepository;
import com.bufet.system.library.response.recipe.GetRecipeListResponse;
import com.bufet.system.library.response.recipe.GetRecipeResponse;
import com.bufet.system.library.service.RecipeService;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.mapstruct.factory.Mappers;
import org.springframework.stereotype.Service;

import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;

/**
 * {@inheritDoc}
 */
@Slf4j
@Service
@RequiredArgsConstructor
public class RecipeServiceImpl implements RecipeService {

    private final RecipeRepository recipeRepository;
    private final RecipeMapper recipeMapper = Mappers.getMapper(RecipeMapper.class);

    /**
     * {@inheritDoc}
     */
    @Override
    public GetRecipeListResponse getRecipes() {
        List<GetRecipeListDto> recipes = new ArrayList<>();

        recipeRepository.findAll()
                .forEach(recipe -> recipes.add(recipeMapper.mapToGetRecipeListDto(recipe)));

        log.info("Return list of recipes: {}", recipes);

        return GetRecipeListResponse.of(recipes);
    }

    @Override
    public GetRecipeResponse getRecipe(@NonNull String id) {
        GetRecipeDto recipeDto = recipeRepository.findById(id)
                .map(recipeMapper::mapToGetRecipeDto)
                .orElseThrow(() -> new RecipeNotExistException(id));

        log.info("Return recipe:{} by give id: {}", recipeDto, id);

        return GetRecipeResponse.of(recipeDto);
    }

    @Override
    public void removeRecipe(@NotNull String id) {

        if (!recipeRepository.existsById(id)) {
            throw new RecipeNotExistException(id);
        }

        log.info("Found recipe with id: {}", id);

        recipeRepository.deleteById(id);

        log.info("Removed recipe with id: {} ", id);

    }
}