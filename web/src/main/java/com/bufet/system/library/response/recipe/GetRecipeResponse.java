package com.bufet.system.library.response.recipe;

/**
 * Buffet System
 * Created by Dawid Cisowski
 * on 25.02.2020.
 */

import com.bufet.system.library.dto.GetRecipeDto;
import com.bufet.system.library.service.RecipeService;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.List;

/**
 * Response model of {@link RecipeService#getRecipe(Long id)} service
 */
@Data
@ApiModel
@AllArgsConstructor(staticName = "of")
public class GetRecipeResponse {

  @ApiModelProperty(value = "Recipe model")
  private GetRecipeDto recipe;
}
