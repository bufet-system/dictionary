#Bufet System: Library Docker file
FROM openjdk:11-slim as runtime
MAINTAINER Dawid.Cisowski@outlook.com

ENV APP_HOME /app
ENV JAVA_OPTS=""
RUN mkdir $APP_HOME

RUN mkdir $APP_HOME/config
RUN mkdir $APP_HOME/log

VOLUME $APP_HOME/log
VOLUME $APP_HOME/config

WORKDIR $APP_HOME
COPY web/target/buffet_library_app.jar app.jar
ENTRYPOINT [ "sh", "-c", "java $JAVA_OPTS -Djava.security.egd=file:/dev/./urandom -jar app.jar" ]

EXPOSE 8091
