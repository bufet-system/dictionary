package com.bufet.system.library.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * Recipe step model
 * <p>
 * Buffet System
 * Created by Dawid Cisowski
 * on 28.09.19.
 */

@Data
@Builder
@Document
@AllArgsConstructor(staticName = "of")
public class RecipeStep {

  @Id
  private String id;

  private String imagePreviewSrc;

  private String content;
}