package com.bufet.system.library.model;


/**
 * List of available difficulty for recipes
 * <p>
 * Buffet System
 * Created by Dawid Cisowski
 * on 28.09.19.
 */

public enum RecipeDifficulty {
  EASY,
  MEDIUM,
  HARD
}
