package com.bufet.system.library.repository;

import com.bufet.system.library.model.Recipe;
import org.springframework.data.repository.CrudRepository;


/**
 * Repository for Recipe {@link Recipe}
 * <p>
 * Buffet System
 * Created by Dawid Cisowski
 * on 28.09.19.
 */

public interface RecipeRepository extends CrudRepository<Recipe, String> {

}