package com.bufet.system.library.model;

import lombok.Builder;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.Duration;
import java.util.List;

/**
 * Recipe model
 * <p>
 * Buffet System
 * Created by Dawid Cisowski
 * on 28.09.19.
 */

@Data
@Builder
@Document
public class Recipe {

  @Id
  private String id;

  private String name;

  private String description;

  private String author;

  private String imagePreviewSrc;

  private List<RecipeIngredient> ingredients;

  private List<RecipeStep> steps;

  private RecipeDifficulty recipeDifficulty;

  /**
   * Time in minutes
   */
  private Long timeToPrepare;

  private List<RecipeTip> tips;
}