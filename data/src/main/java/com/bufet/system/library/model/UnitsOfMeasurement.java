package com.bufet.system.library.model;


/**
 * List of available units of measurement in recipes
 * <p>
 * Buffet System
 * Created by Dawid Cisowski
 * on 28.09.19.
 */

public enum UnitsOfMeasurement {
  GRAM,
  KILOGRAM,
  LITER,
  MILILITER,
  ITEM,
  CUP,
  TEA_SPOON
}
